package utils;

public class WritableSdNode {
	public String id;
	public String name;
	
	public double xHat; // true value
	public double xBar; // init expected value
	public double yBar; // log init expected value
	public double yHat;// log true value
	
	public double meanProba;
	public double marginProba;
	public double[] logProbaOfScales;
	public double[][] logProbaOfScalesCondToParent;
	
	
	public double[] marginScaleProbaCond;

}
