package utils;

import model.DoublePair;

public class MathOperations {
	//private static double temp1, temp2;
	private static double log2Val = Math.log(2);

	public static double log2(double nb) {
		return Math.log(nb) / log2Val;
	}

	public static double logBase(double base, double nb) {
		return Math.log(nb) / Math.log(base);
	}

	public static double getLogGeometric(double param, int value) {
		if (value < 0) {
			return -Double.MAX_VALUE; // means infinity
		}
		
		if (param==1) {
			if (value==0) {
				return 0;
			}
			else {
				return -Double.MAX_VALUE;
			}
		}
		
		return value*Math.log(1-param)+Math.log(param);
	}
	
	
	
	public static double getGeometric(double param, int value) {
		if (value < 0) {
			return 0;
		}
		double temp1=Math.pow(1 - param, value) * param;
		if (Double.isInfinite(temp1)) {
			throw new RuntimeException("comb is infinite");
		}
		if (Double.isNaN(temp1)) {
			throw new RuntimeException("comb is nan");
		}
		return temp1;
	}

	public static double getCdfGeometric(double param,double value) {
		if (value<0) {
			return 0;
		}
		double temp1=1.-Math.pow(1.-param, value+1);
		if (Double.isInfinite(temp1)) {
			throw new RuntimeException("comb is infinite");
		}
		if (Double.isNaN(temp1)) {
			throw new RuntimeException("comb is nan");
		}
		return temp1;
	}
	public static double getLogCdfIntervalGeometric(double param,double minKIncluded,double maxKExcluded) {
		if (minKIncluded>=maxKExcluded) {
			return -Double.MAX_VALUE; // 1 means -infinity
		}
		if (param==1) {
			if (minKIncluded==0) {
				return 0;
			}
			else {
				return -Double.MAX_VALUE;
			}
		}
		double temp=1.-Math.pow(1-param, maxKExcluded-minKIncluded);
		return minKIncluded*Math.log(1-param)+Math.log(temp);
	}
	
	
	
	
	
	public static double getCdfIntervalGeometric(double param,double minKIncluded,double maxKExcluded) {
		double temp1=getCdfGeometric(param, maxKExcluded-1);
		double temp2;
		if (minKIncluded==0) {
			temp2=0;
		}
		else {
			temp2=getCdfGeometric(param, minKIncluded-1);
		}
		return temp1-temp2;
	}
	
	
//	public static double getBinomialWithPreComputedComb(int n, int k, double p, double comb) {
//		if (k < 0 || n < 0 || k > n) {
//			return 0;
//		}
//		return comb * Math.pow(p, k) * Math.pow(1. - p, n - k);
//	}
	
	
	public static DoublePair getLogCdfBinomial(double n, double k, double p,DesignPoint dp) {
		if (k<0 || n<0 || p<0  || p>1) {
			throw new RuntimeException("n and k must be positive and p must be between 0 and 1");
		}
		//long startTime = System.nanoTime();
		DoublePair temp1=Beta.logRegularizedIncompleteBetaFunction(n - k, k + 1, 1 - p);
		//long endTime = System.nanoTime();
		//dp.statistics.timeBinomial+=(endTime-startTime);
		dp.statistics.nbCallsBetaFunction++;
		if (Double.isInfinite(temp1.first) || Double.isInfinite(temp1.second)) {
			throw new RuntimeException("comb is infinite");
		}
		if (Double.isNaN(temp1.first) || Double.isNaN(temp1.second)) {
			throw new RuntimeException("comb is nan");
		}
		return temp1;
	}
	
	
	
	
	public static double getCdfBinomial(double n, double k, double p) {
		if (k<0 || n<0 || p<0  || p>1) {
			throw new RuntimeException("n and k must be positive and p must be between 0 and 1");
		}
		if (k>=n) {
			return 1;
		}
		double temp1=Beta.regularizedIncompleteBetaFunction(n - k, k + 1, 1 - p);
		if (Double.isInfinite(temp1)) {
			throw new RuntimeException("comb is infinite");
		}
		if (Double.isNaN(temp1)) {
			throw new RuntimeException("comb is nan");
		}
		return temp1;
	}
	static DoublePair tempD1=new DoublePair(0., 0.);
	static DoublePair tempD2=new DoublePair(0., 0.);
	public static double getLogCdfIntervalBinomial(double n,double p, double minKIncluded,double maxKExcluded,DesignPoint dp) {
		
		
		
		if (maxKExcluded<=minKIncluded) {
			return -Double.MAX_VALUE;
		}
		
		if (maxKExcluded-1>=n) {
			tempD1.first=0.;
			tempD1.second=-Double.MAX_VALUE;
		}
		else {
			DoublePair tt=getLogCdfBinomial(n, maxKExcluded-1, p,dp);
			tempD1.first=tt.first;
			tempD1.second=tt.second;
			
		}
		
		if (minKIncluded-1<0) {
			tempD2.first=-Double.MAX_VALUE;
			tempD2.second=0.;
		}
		else {
			//long startTime = System.nanoTime();
			DoublePair tt=getLogCdfBinomial(n, minKIncluded-1, p,dp);
			tempD2.first=tt.first;
			tempD2.second=tt.second;
			//long endTime = System.nanoTime();
			//dp.statistics.timeBinomial+=(endTime-startTime);
		}
		double temp;
		
		if (((maxKExcluded+1.)/(n+3))>p) {
			// work with 1-F(b) CDF
			temp=tempD2.second;
			double temp3=0;
			if (tempD1.second!=-Double.MAX_VALUE) {
				temp3=Math.log(1.-Math.exp(tempD1.second-tempD2.second));
			}
			temp=temp+temp3;
		}
		else {
			// work with F(b)
			temp=tempD1.first;
			double temp3=0;
			if (tempD2.first!=0) {
				temp3=Math.log(1-Math.exp(tempD2.first-tempD1.first));
			}
			temp=temp+temp3;
		}
		return temp;
	}
	
	
	public static double getCdfIntervalBinomial(double n,double p, double minKIncluded,double maxKExcluded) {
		double temp1,temp2;
		if (maxKExcluded-1>=n) {
			temp1=1;
		}
		else {
			//temp1=Binomial.cdf((int)maxKExcluded-1,(int) n, p);
			temp1=getCdfBinomial(n, maxKExcluded-1, p);
		}
		
		if (minKIncluded==0) {
			temp2=0;
		}
		else {
			//temp2=Binomial.cdf( (int)minKIncluded-1,(int)n, p);
			temp2=getCdfBinomial(n, minKIncluded-1, p);
		}
		return temp1-temp2;
	}

	public static double getBinomial(double n, double k, double p) {
		if (k < 0 || n < 0 || k > n) {
			return 0;
		}
		double temp1=k*Math.log(p)+(n-k)*Math.log(1.-p)+getLogNChooseK(n, k);
		
		temp1=Math.exp(temp1);
		if (Double.isInfinite(temp1)) {
			throw new RuntimeException("comb is infinite");
		}
		if (Double.isNaN(temp1)) {
			throw new RuntimeException("comb is nan");
		}
		return temp1;
	}

	public static double getLogNChooseK(double n, double k) {
		if (k > n) {
			throw new RuntimeException("calculation not possible");
		}
		if (k < 0 || n < 0) {
			throw new RuntimeException("calculation not possible");
		}
		if (k == 0 || k == n) {
			return 0;
		}

		if (n <= 10) {
			double comb = 1;

			for (double i = 1; i <= n - k; i++) {
				comb /= i;
			}
			for (double i = n; i > k; i--) {
				comb *= i;
			}

			if (Double.isInfinite(comb)) {
				throw new RuntimeException("comb is infinite");
			}
			if (Double.isNaN(comb)) {
				throw new RuntimeException("comb is nan");
			}
			return Math.log(comb);
		}
		else {
			double logComb=Gamma.logGamma(n+1)-Gamma.logGamma(k+1)-Gamma.logGamma(n-k+1);
			return logComb;
		}
	}
}
