package utils;

import java.util.ArrayList;

import model.SdNode;
import model.SubDag;

public class WritableSubDag {
	public DesignPoint metadata;
	public ArrayList<WritableSdNode> sdnodes=new ArrayList<>(); 
	
	
	public static WritableSubDag create(SubDag subdag,DesignPoint designPoint) {
		WritableSubDag ws=new WritableSubDag();
		ws.metadata=designPoint;
		for (SdNode sdnode: subdag.sdnodes) {
			WritableSdNode wsn=new WritableSdNode();
			wsn.id=sdnode.node.id;
			wsn.name=sdnode.node.name;
			wsn.xBar=sdnode.xBar;
			wsn.xHat=sdnode.xHat;
			wsn.yHat=sdnode.yHat;
			wsn.yBar=sdnode.yBar;
			wsn.meanProba=sdnode.meanProba;
			wsn.logProbaOfScalesCondToParent=sdnode.logProbaOfScalesCondToParent;
			wsn.logProbaOfScales=sdnode.logProbaOfScales;
			wsn.marginScaleProbaCond=sdnode.marginScaleProbaCond;
			wsn.marginProba=sdnode.marginProba;
			
			ws.sdnodes.add(wsn);
		}
		return ws;
	}
}
