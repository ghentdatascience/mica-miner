package utils;

public class Statistics {
	public int nbCallsFactorFunction=0;
	public long totalTimeMS=0;
	public long timePerNodeMS=0;
	public long timeBinomial=0;
	public long nbCallsBetaFunction=0;
	public long memoryIncludingFreeKB=0;
	public long netMemoryInKB=0;
}
