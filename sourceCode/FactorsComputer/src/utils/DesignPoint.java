package utils;

public class DesignPoint {
	//public String dagPath="countsDataAnimals/DAG.txt";
	//public String itemNamesPath="countsDataAnimals/itemsNames.txt";
	//public String beliefSgPath="countsDataAnimals/AU/1970.txt";
	//public String minedSgPath="countsDataAnimals/AU/2000.txt";
	//public String resultsPath="countsDataAnimals/AU/probasModelOff.json";
	//public DistType distType=DistType.binomial;
	//public String dagPath="LondonAmsterdam/DAG.txt";
	//public String itemNamesPath="LondonAmsterdam/itemsNames.txt";
	//public String beliefSgPath="LondonAmsterdam/supportLondon.txt";
	//public String minedSgPath="LondonAmsterdam/supportAmsterdam.txt";
	//public String resultsPath="LondonAmsterdam/probasModel.json";
	//public DistType distType=DistType.binomial;
	//public String dagPath="VotesUEFiltered/DAG.txt";
	//public String itemNamesPath="VotesUEFiltered/itemsNames.txt";
	//public String beliefSgPath="VotesUEFiltered/supportsEPD7DEDUP.txt";
	//public String minedSgPath="VotesUEFiltered/supportsEPD8DEDUP.txt";
	//public String resultsPath="VotesUEFiltered/probasModel.json";
	public String dagPath="CityVsAll/DAG.txt";
	public String itemNamesPath="CityVsAll/itemsNames.txt";
	public String beliefSgPath="CityVsAll/supportAll.txt";
	public String minedSgPath="CityVsAll/supportRome.txt";
	public String resultsPath="CityVsAll/probasModel_Rome_Small.json";
	
	public int maxScaleExponent=-1;
	public double scaleBase=2.;
	//public int maxX=3000000;
	public int maxX=1000;
	public boolean normalizeRoot=true;
	
	public Statistics statistics=new Statistics();
	
	public DesignPoint() {
		
	}
	
	public void updateScale() {
		maxScaleExponent=(int)Math.ceil(MathOperations.logBase(scaleBase, maxX));
		// adjust maxX
		maxX=(int)Math.pow(scaleBase, maxScaleExponent);
		if (maxX>Constants.maxAllowedX) {
			throw new RuntimeException("maxX exceeds its max allowed value");
		}
		System.out.println("maxX:"+maxX);
	}
	
}
