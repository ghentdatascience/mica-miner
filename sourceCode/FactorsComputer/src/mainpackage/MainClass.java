package mainpackage;


import controller.DagBuilder;
import controller.QualityComputer;
import controller.ResultsWriter;
import controller.ScaleProbaComputer;
import controller.SubgroupBuilder;
import model.DLType;
import model.Dag;
import model.SubDag;
import model.Subgroup;
import utils.DesignPoint;
import utils.Utilities;

public class MainClass {
	static long kb=1024;

	public static void main(String[] args) {
		Runtime runtime = Runtime.getRuntime();
		DesignPoint designPoint=new DesignPoint();
		if (args.length != 1) {
			System.out.println("you can specify one parameter : parameters File path. Otherwise, default values will be considered");
		}
		else {
			Utilities.readParametersFromFile(designPoint, args[0]);
		}
		designPoint.updateScale();
		//String dagPath="DAG.txt";
		//String itemNamesPath="itemsNames.txt";
		//String londonPath="supportLondon.txt";
		//String parisPath="supportAmsterdam.txt";
		Dag dag=new DagBuilder(designPoint.itemNamesPath, designPoint.dagPath).build();
		Subgroup beliefSg=new SubgroupBuilder(designPoint.beliefSgPath, dag).build();
		Subgroup minedSg=new SubgroupBuilder(designPoint.minedSgPath, dag).build();
		System.out.println("loaded");
		
		ScaleProbaComputer miner=new ScaleProbaComputer(dag, beliefSg, minedSg, new QualityComputer(designPoint));
		miner.buildAllSdNodes();
		long startTime = System.nanoTime()/1000000;
		SubDag subdag=miner.computeAllProbas(DLType.uniform);
		long endTime = System.nanoTime()/1000000;
		designPoint.statistics.totalTimeMS=endTime-startTime;
		designPoint.statistics.timePerNodeMS=designPoint.statistics.totalTimeMS/minedSg.supportList.size();
		//ArrayList<SubDag> subdags=miner.computeAntiChainsWithSimpleUpdate(DLType.mdl);
		designPoint.statistics.timeBinomial/=1000000;
		designPoint.statistics.memoryIncludingFreeKB=runtime.totalMemory()/kb;
		designPoint.statistics.netMemoryInKB=(runtime.totalMemory()- runtime.freeMemory())/kb;
		ResultsWriter.writeSubDag(subdag, designPoint.resultsPath,designPoint);
		//ResultsWriter.writeSubDags(subdags, designPoint.resultsPath);
		System.out.println("sdnodes built");
	}

}
