package controller;

import java.util.ArrayList;
import java.util.HashMap;

import model.DLType;
import model.Dag;
import model.DistType;
import model.Node;
import model.Pair;
import model.SdNode;
import model.Subgroup;
import utils.DesignPoint;
import utils.MathOperations;

public class QualityComputer {
	public DesignPoint designPoint;

	public QualityComputer(DesignPoint designPoint) {
		super();
		this.designPoint = designPoint;
	}

	// public double icNode(Node node)

	public void computeExpectedValuesAndAdjust(SdNode sdNode, double rootV, double rootExpectedV, Subgroup groundTruth,
			Subgroup subgroup) {
		if (subgroup.supportMap.containsKey(sdNode.node)) {
			sdNode.xHat = subgroup.supportMap.get(sdNode.node).second+1;
		} else {
			Pair<Node, Double> pair = new Pair<>(null,0.);
			pair.first = sdNode.node;
			pair.second = 1.;
			subgroup.supportMap.put(sdNode.node, pair);
			sdNode.xHat = 1;
		}
		double groundV = 1;
		if (groundTruth.supportMap.containsKey(sdNode.node)) {
			groundV = groundTruth.supportMap.get(sdNode.node).second+1;
			if (designPoint.normalizeRoot) {
				groundV*=(rootV/rootExpectedV);
			}
		}
		sdNode.xBar = groundV;
		if (sdNode.xHat > 0 && sdNode.xBar <= 1) {
			sdNode.xBar = 1;
			double gV = sdNode.xBar;
			if (groundTruth.supportMap.containsKey(sdNode.node)) {
				Pair<Node, Double> pair = groundTruth.supportMap.get(sdNode.node);
				pair.second = gV;
			} else {
				Pair<Node, Double> pair = new Pair<>(null,0.);
				pair.first = sdNode.node;
				pair.second = gV;
				groundTruth.supportMap.put(pair.first, pair);
				groundTruth.supportList.add(pair);
			}
		}
		sdNode.yHat=MathOperations.logBase(designPoint.scaleBase, sdNode.xHat);
		sdNode.yBar=MathOperations.logBase(designPoint.scaleBase, sdNode.xBar);
		
	}


}
