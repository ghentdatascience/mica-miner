package controller;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import model.SubDag;
import utils.DesignPoint;
import utils.WritableSubDag;

public class ResultsWriter {
	
	
	
	public static void writeSubDag(SubDag sdag, String filePath,DesignPoint designPoint) {
		WritableSubDag ws=WritableSubDag.create(sdag,designPoint);
		Gson gson;
		gson = new GsonBuilder().setPrettyPrinting().create();
		try (FileWriter writer = new FileWriter(
				filePath)) {
			gson.toJson(ws, writer);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static void writeSubDags(ArrayList<SubDag> sdags, String filePath,DesignPoint designPoint) {
		ArrayList<WritableSubDag> wsds=new  ArrayList<>();
		for (SubDag sdag : sdags) {
			wsds.add(WritableSubDag.create(sdag,designPoint));
		}
		Gson gson;
		gson = new GsonBuilder().setPrettyPrinting().create();
		try (FileWriter writer = new FileWriter(
				filePath)) {
			gson.toJson(wsds, writer);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
