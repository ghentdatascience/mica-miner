package controller;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import model.Dag;
import model.Node;

public class DagBuilder {
	private String itemNamesPath;
	private String dagPath;

	public DagBuilder(String itemNamesPath, String dagPath) {
		this.itemNamesPath = itemNamesPath;
		this.dagPath = dagPath;
	}

	public Dag build() {
		Dag dag = new Dag();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(itemNamesPath));
			String line;
			int cpt=0;
			while ((line=reader.readLine())!=null) {
				line=line.replace("\n", "").replace("\r", "");
				String[] elements=line.split(",");
				Node node=new Node();
				node.id=elements[0];
				node.name=elements[1];
				node.index=cpt;
				dag.nodesMap.put(node.id, node);
				dag.allNodes.add(node);
				if (cpt==0) {
					dag.root=node;
				}
				cpt++;
			}
			reader.close();
			reader = new BufferedReader(new FileReader(dagPath));
			cpt=0;
			while ((line=reader.readLine())!=null) {
				cpt++;
				if (cpt>1) {
					line=line.replace("\n", "").replace("\r", "");
					String[] elements=line.split(",");
					Node parent=dag.nodesMap.get(elements[0]);
					Node child=dag.nodesMap.get(elements[1]);
					parent.directDesc.add(child);
					child.directParent=parent;
				}
			}
			reader.close();
			for (Node node : dag.allNodes) {
				if (node.directDesc.size()==0) {
					dag.leaves.add(node);
				}
			}
			dag.computeAllDesc();
			dag.computeAllAsc();
			dag.setChildrenIndices();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dag;
	}
}
