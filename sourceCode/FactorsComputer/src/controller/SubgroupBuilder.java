package controller;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import model.Dag;
import model.Node;
import model.Pair;
import model.Subgroup;

public class SubgroupBuilder {
	private String subgroupPath;
	private Dag dag;
	
	public SubgroupBuilder(String subgroupPath, Dag dag) {
		this.subgroupPath = subgroupPath;
		this.dag = dag;
	}
	
	public Subgroup build() {
		Subgroup sg=new Subgroup();
		sg.overalNbNodesInDag=dag.allNodes.size();
		try {
			BufferedReader reader=new BufferedReader(new FileReader(subgroupPath));
			String line;
			while ((line=reader.readLine())!=null) {
				line=line.replace("\r", "").replace("\n","");
				String[] elements=line.split(",");
				Node n=dag.nodesMap.get(elements[0]);
				Pair<Node,Double> pair=new Pair<>(null,0.);
				pair.first=n;
				pair.second=Double.parseDouble(elements[1]);
				sg.supportList.add(pair);
				sg.supportMap.put(pair.first, pair);
			}
			reader.close();
			sg.computeOccuredNodes();
			//sg.computeSumOfSupports();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sg;
	}
}
