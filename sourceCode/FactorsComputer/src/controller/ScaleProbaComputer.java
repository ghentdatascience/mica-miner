package controller;

import java.util.ArrayList;
import java.util.HashMap;

import model.DLType;
import model.Dag;
import model.Node;
import model.SdNode;
import model.SubDag;
import model.Subgroup;

public class ScaleProbaComputer {
	private Dag dag;
	private Subgroup groundTruth;
	private Subgroup subgroup;
	public ArrayList<SdNode> allSdNodes;
	public HashMap<Node, SdNode> allSdNodesMap;
	private QualityComputer qualComputer;

	public ScaleProbaComputer(Dag dag, Subgroup groundTruth, Subgroup subgroup, QualityComputer qualComputer) {
		this.dag = dag;
		this.groundTruth = groundTruth;
		this.subgroup = subgroup;
		this.qualComputer = qualComputer;
	}

	public void buildAllSdNodes() {
		allSdNodes = new ArrayList<>();
		allSdNodesMap = new HashMap<>();
		double rootV = subgroup.supportMap.get(dag.root).second;
		double rootExpectedV = groundTruth.supportMap.get(dag.root).second;
		for (Node node : dag.allNodes) {
			if (groundTruth.supportMap.containsKey(node) || subgroup.supportMap.containsKey(node)) {
				SdNode sdnode = new SdNode();
				sdnode.node = node;
				qualComputer.computeExpectedValuesAndAdjust(sdnode, rootV, rootExpectedV, groundTruth, subgroup);
				allSdNodes.add(sdnode);
				allSdNodesMap.put(node, sdnode);
			}
		}
		groundTruth.computeSumOfSupports();
		subgroup.computeSumOfSupports();
	}

	
	

	public SubDag computeAllProbas(DLType dl) {
		SubDag subdag = new SubDag();
		subdag.sdnodes=allSdNodes;
		SdNode rootSdn=allSdNodesMap.get(dag.root);
		rootSdn.computeLogProbas(null, allSdNodesMap, qualComputer.designPoint,0);
		qualComputer.designPoint.statistics.nbCallsFactorFunction=SdNode.nbCallFactorFunction;
		return subdag;
	}

}


