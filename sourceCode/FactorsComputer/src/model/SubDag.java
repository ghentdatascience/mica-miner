package model;

import java.util.ArrayList;

public class SubDag {
	
	
	public ArrayList<SdNode> sdnodes=new ArrayList<>(); // if v>expectation
	//public ArrayList<SdNode> negativeNodes=new ArrayList<>(); // if v<expectation	
	//public ArrayList<SdNode> neutralNodes=new ArrayList<>();  // if v==expectation
	
	public double ic=0;
	public double dl=0;
	public double si=0;
	
	// not sure if we need these two function, 
	// since we will check these constraints by SubDag construction
//	public boolean isAntiChain() {
//		
//	}
//	public boolean isCovering() {
//		
//	}
}
