package model;

import java.util.HashMap;

import utils.DesignPoint;
import utils.MathOperations;

public class SdNode {// implements Comparable<SdNode> {
	private static double binomialMaxP=0.99;
	public Node node;
	public double xHat; // true value
	public double xBar; // init expected value
	public double yBar; // init log expected value
	public double yHat; // log true value

	public double[][] logProbaOfScalesCondToParent = null;
	public double[] logProbaOfScales = null;
//	public double[] probaOfValues = null;

	public double[] marginScaleProbaCond = null;
	public double marginProba = 0;
	public double meanProba;

	public static int nbCallFactorFunction = 0;
	public double binomialParam;
	public double geometricParam;
	public static double[] temp;
	// private double lastCombinationValue=-1;

	public int computeLogProbas(SdNode parentSdn, HashMap<Node, SdNode> allSdNodesMap, DesignPoint designPoint,
			int curIndex) {
		if (temp==null) {
			temp=new double[designPoint.maxX];
		}
//		for (int i=0;i<temp.length;i++) {
//			temp[i]=0;
//		}
		if (parentSdn != null) {
			binomialParam = xBar / parentSdn.xBar;
			if (binomialParam>binomialMaxP) {
				binomialParam=binomialMaxP;
			}
		}
		geometricParam = 1. / (1. + xBar);
		logProbaOfScalesCondToParent = new double[designPoint.maxScaleExponent][];
		for (int i = 0; i < designPoint.maxScaleExponent; i++) {
			logProbaOfScalesCondToParent[i] = new double[designPoint.maxScaleExponent];
		}
		// probaOfValues = new double[designPoint.maxX];
		logProbaOfScales = new double[designPoint.maxScaleExponent];
		marginScaleProbaCond = new double[designPoint.maxScaleExponent];
		marginProba = 0;
		meanProba = xBar;
		int newIndex = curIndex + 1;
		System.out.println("curIndex:" + newIndex);
		double minKprim, maxKprim, minK, maxK;
		// we first compute probaOfScales
		for (int kprim = 0; kprim < designPoint.maxScaleExponent; kprim++) {
			if (kprim == 0) {
				minKprim = 0;
				maxKprim = designPoint.scaleBase;
			} else {
				minKprim = Math.pow(designPoint.scaleBase, kprim);
				maxKprim = minKprim * designPoint.scaleBase;
			}
			logProbaOfScales[kprim] = MathOperations.getLogCdfIntervalGeometric(geometricParam, minKprim, maxKprim);
			if (logProbaOfScales[kprim]<=0) {
				marginProba+=Math.exp(logProbaOfScales[kprim]);
			}
			
		}
		// we second compute probaOfScalesCondToParent
		if (parentSdn != null) {
			for (int kprim = 0; kprim < designPoint.maxScaleExponent; kprim++) {
				if (kprim == 0) {
					minKprim = 0;
					maxKprim = designPoint.scaleBase;
				} else {
					minKprim = Math.pow(designPoint.scaleBase, kprim);
					maxKprim = minKprim * designPoint.scaleBase;
				}
				for (int k=0;k<kprim;k++) {
					logProbaOfScalesCondToParent[kprim][k]=-Double.MAX_VALUE;
				}
				for (int k = kprim; k < designPoint.maxScaleExponent; k++) {
					if (parentSdn.logProbaOfScales[k] > -Double.MAX_VALUE) {
						if (k == 0) {
							minK = 0;
							maxK = designPoint.scaleBase;
						} else {
							minK = Math.pow(designPoint.scaleBase, k);
							maxK = minK * designPoint.scaleBase;
						}
						double tmp = 0;
						double maxLP=-Double.MAX_VALUE;
						int indexMax=0;
						int limitJ=(int)minK;
						for (int j = (int) minK; j < maxK; j++) {
							limitJ=j+1;
							temp[j-(int)minK]=MathOperations.getLogGeometric(parentSdn.geometricParam, j);
							
							temp[j-(int)minK]+=MathOperations.getLogCdfIntervalBinomial(j, binomialParam, minKprim, maxKprim,designPoint);
							
							if (temp[j-(int)minK]>maxLP) {
								maxLP=temp[j-(int)minK];
								indexMax=j-(int)minK;
							}
							if (maxLP-temp[j-(int)minK]>800) {
								// TODO
								// attention, this optimization is not proven yet
								// we need to prove that the distribution in scales has one local optimum which is global
								break;
							}
						}
						temp[indexMax]=temp[(int)(limitJ-minK)-1];
						temp[(int)(limitJ-minK)-1]=maxLP;
						//Arrays.sort(temp, 0, (int)(maxK-minK));
						tmp=temp[(int)(limitJ-minK)-1];
						
						if (tmp!=-Double.MAX_VALUE || tmp!=0) {
							double tmp2=0;
							for (int y=0;y<limitJ-minK-1;y++) {
								tmp2+=Math.exp(temp[y]-tmp);
							}
							tmp2++; // this is +1 !!!
							tmp+=Math.log(tmp2);
						}
						
						
						logProbaOfScalesCondToParent[kprim][k] = tmp - parentSdn.logProbaOfScales[k];
						marginScaleProbaCond[k]+=Math.exp(logProbaOfScalesCondToParent[kprim][k]);
					}
				}
			}
		}

		for (Node child : node.directDesc) {
			if (allSdNodesMap.containsKey(child)) {
				SdNode childSdn = allSdNodesMap.get(child);
				newIndex = childSdn.computeLogProbas(this, allSdNodesMap, designPoint, newIndex);
			}
		}
		return newIndex;
	}

//	@Override
//	public int compareTo(SdNode o) {
//		if (xBar != xHat && o.xBar == o.xHat) {
//			return 1;
//		}
//		if (xBar == xHat && o.xBar != o.xHat) {
//			return -1;
//		}
//		if (si > o.si)
//			return 1;
//		if (si < o.si)
//			return -1;
//		return 0;
//	}

}
