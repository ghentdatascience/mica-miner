package model;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.lucene.util.OpenBitSet;

public class Subgroup {
	public ArrayList<Pair<Node,Double>> supportList=new ArrayList<>();
	public HashMap<Node,Pair<Node,Double>> supportMap=new HashMap<>();
	public OpenBitSet occuredNodes=null;
	public int overalNbNodesInDag=-1;
	//public double sumOfSupports=-1;
	public void computeOccuredNodes() {
		occuredNodes=new OpenBitSet(overalNbNodesInDag);
		for (Pair<Node,Double> pair : supportList) {
			occuredNodes.fastSet(pair.first.index);
		}
	}
	public void computeSumOfSupports() {
//		sumOfSupports=0;
//		for (Pair<Node,Double> pair : supportList) {
//			sumOfSupports+=pair.second;
//		}
	}
}
