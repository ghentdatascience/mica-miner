package model;

import java.util.ArrayList;
import java.util.HashMap;

public class Dag {
	public Node root;  
	public ArrayList<Node> allNodes=new ArrayList<>();
	public ArrayList<Node> leaves=new ArrayList<>();
	public HashMap<String,Node> nodesMap=new HashMap<>();
	
	
	public void computeAllDesc() {
		root.computeAllDesc(allNodes.size());
	}
	public void computeAllAsc() {
		for (Node n: leaves) {
			n.computeAllAsc(allNodes.size());
		}
	}
	
	public void setChildrenIndices() {
		root.setChildrenIndices();
	}
	
}
