package mainpackage;

import java.util.ArrayList;

import controller.DagBuilder;
import controller.HierarchyMiner;
import controller.QualityComputer;
import controller.ResultsWriter;
import controller.SgAndSdNodesBuilder;
import model.DLType;
import model.Dag;
import model.SubDag;
import utils.DesignPoint;
import utils.Utilities;

public class MainClass {
	static long kb=1024;

	public static void main(String[] args) {
		Runtime runtime = Runtime.getRuntime();
		DesignPoint designPoint=new DesignPoint();
		
		if (args.length != 1) {
			System.out.println("you can specify one parameter : parameters File path. Otherwise, default values will be considered");
		}
		else {
			Utilities.readParametersFromFile(designPoint, args[0]);
		}
		
		//String dagPath="DAG.txt";
		//String itemNamesPath="itemsNames.txt";
		//String londonPath="supportLondon.txt";
		//String parisPath="supportAmsterdam.txt";
		Dag dag=new DagBuilder(designPoint.itemNamesPath, designPoint.dagPath).build();
		SgAndSdNodesBuilder sgAndSdNodesBuilder=new SgAndSdNodesBuilder(dag, designPoint.sgAndSdnodeFilePath);
		sgAndSdNodesBuilder.load(designPoint);
		System.out.println("loaded");
		
		
		long startTime = System.nanoTime()/1000000;
		HierarchyMiner miner=new HierarchyMiner(sgAndSdNodesBuilder, new QualityComputer(designPoint), DLType.uniform);
		//SubDag sd=miner.getAllSdNodesInSubDag();
		//SubDag sd=miner.getAllSdNodesInSubDagWithUpdate();
		//SubDag sd=miner.getMaximalAntichain();
		
		ArrayList<SubDag> allAntiChains=miner.getNotNecMaximalAntichains();
		long endTime = System.nanoTime()/1000000;
		designPoint.statistics.totalTimeMS=endTime-startTime;
		designPoint.statistics.memoryIncludingFreeKB=runtime.totalMemory()/kb;
		designPoint.statistics.netMemoryInKB=(runtime.totalMemory()- runtime.freeMemory())/kb;
		//designPoint.statistics.timePerNodeMS=designPoint.statistics.totalTimeMS/minedSg.supportList.size();

		//ResultsWriter.writeSubDag(sd, designPoint.resultsPath,designPoint);
		ResultsWriter.writeSubDags(allAntiChains, designPoint.resultsPath,designPoint);
		System.out.println("sdnodes built");
	}

}
