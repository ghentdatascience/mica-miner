package utils;

public class WritableSdNode {
	public String id;
	public String name;
	
	public double xHat; // true value
	public double xBar; // init expected value
	public double yBar; // log init expected value
	public double yHat;// log true value
	public double currentMode;
	public double marginProba;
	public double[] logMarginals;
	public double observedValue;
	public double logProba;
	public double ic;
	public double dl;
	public double si;
	public boolean isObserved ;

	
}
