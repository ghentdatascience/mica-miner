package utils;

public class MathOperations {
	//private static double temp1, temp2;
	private static double log2Val = Math.log(2);

	public static double log2(double nb) {
		return Math.log(nb) / log2Val;
	}

	public static double logBase(double base, double nb) {
		return Math.log(nb) / Math.log(base);
	}

	
	
	public static double getGeometric(double param, int value) {
		if (value < 0) {
			return 0;
		}
		double temp1=Math.pow(1 - param, value) * param;
		if (Double.isInfinite(temp1)) {
			throw new RuntimeException("comb is infinite");
		}
		if (Double.isNaN(temp1)) {
			throw new RuntimeException("comb is nan");
		}
		return temp1;
	}

	public static double getCdfGeometric(double param,double value) {
		if (value<0) {
			return 0;
		}
		double temp1=1.-Math.pow(1.-param, value+1);
		if (Double.isInfinite(temp1)) {
			throw new RuntimeException("comb is infinite");
		}
		if (Double.isNaN(temp1)) {
			throw new RuntimeException("comb is nan");
		}
		return temp1;
	}
	public static double getCdfIntervalGeometric(double param,double minKIncluded,double maxKExcluded) {
		double temp1=getCdfGeometric(param, maxKExcluded-1);
		double temp2;
		if (minKIncluded==0) {
			temp2=0;
		}
		else {
			temp2=getCdfGeometric(param, minKIncluded-1);
		}
		return temp1-temp2;
	}
	
	
//	public static double getBinomialWithPreComputedComb(int n, int k, double p, double comb) {
//		if (k < 0 || n < 0 || k > n) {
//			return 0;
//		}
//		return comb * Math.pow(p, k) * Math.pow(1. - p, n - k);
//	}
	
	
	
	
	
	
	
	public static double getCdfBinomial(double n, double k, double p) {
		if (k<0 || n<0 || p<0  || p>1) {
			throw new RuntimeException("n and k must be positive and p must be between 0 and 1");
		}
		if (k>=n) {
			return 1;
		}
		double temp1=Beta.regularizedIncompleteBetaFunction(n - k, k + 1, 1 - p);
		if (Double.isInfinite(temp1)) {
			throw new RuntimeException("comb is infinite");
		}
		if (Double.isNaN(temp1)) {
			throw new RuntimeException("comb is nan");
		}
		return temp1;
	}
	
	public static double getCdfIntervalBinomial(double n,double p, double minKIncluded,double maxKExcluded) {
		double temp1,temp2;
		if (maxKExcluded-1>=n) {
			temp1=1;
		}
		else {
			//temp1=Binomial.cdf((int)maxKExcluded-1,(int) n, p);
			temp1=getCdfBinomial(n, maxKExcluded-1, p);
		}
		
		if (minKIncluded==0) {
			temp2=0;
		}
		else {
			//temp2=Binomial.cdf( (int)minKIncluded-1,(int)n, p);
			temp2=getCdfBinomial(n, minKIncluded-1, p);
		}
		return temp1-temp2;
	}

	public static double getBinomial(double n, double k, double p) {
		if (k < 0 || n < 0 || k > n) {
			return 0;
		}
		double temp1=k*Math.log(p)+(n-k)*Math.log(1.-p)+getLogNChooseK(n, k);
		
		temp1=Math.exp(temp1);
		if (Double.isInfinite(temp1)) {
			throw new RuntimeException("comb is infinite");
		}
		if (Double.isNaN(temp1)) {
			throw new RuntimeException("comb is nan");
		}
		return temp1;
	}

	public static double getLogNChooseK(double n, double k) {
		if (k > n) {
			throw new RuntimeException("calculation not possible");
		}
		if (k < 0 || n < 0) {
			throw new RuntimeException("calculation not possible");
		}
		if (k == 0 || k == n) {
			return 0;
		}

		if (n <= 10) {
			double comb = 1;

			for (double i = 1; i <= n - k; i++) {
				comb /= i;
			}
			for (double i = n; i > k; i--) {
				comb *= i;
			}

			if (Double.isInfinite(comb)) {
				throw new RuntimeException("comb is infinite");
			}
			if (Double.isNaN(comb)) {
				throw new RuntimeException("comb is nan");
			}
			return Math.log(comb);
		}
		else {
			double logComb=Gamma.logGamma(n+1)-Gamma.logGamma(k+1)-Gamma.logGamma(n-k+1);
			return logComb;
		}
	}
}
