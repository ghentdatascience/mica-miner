package utils;

import java.util.ArrayList;

import model.SdNode;
import model.SubDag;

public class WritableSubDag {
	//public DesignPoint metadata;
	public ArrayList<WritableSdNode> sdnodes=new ArrayList<>(); 
	
	
	public static WritableSubDag create(SubDag subdag) {
		WritableSubDag ws=new WritableSubDag();
		//ws.metadata=designPoint;
		for (SdNode sdnode: subdag.sdnodes) {
			WritableSdNode wsn=new WritableSdNode();
			wsn.id=sdnode.node.id;
			wsn.name=sdnode.node.name;
			wsn.xBar=sdnode.xBar;
			wsn.xHat=sdnode.xHat;
			wsn.yHat=sdnode.yHat;
			wsn.yBar=sdnode.yBar;
			wsn.currentMode=sdnode.currentMode;
			wsn.marginProba=sdnode.marginProba;
			wsn.logMarginals=sdnode.logMarginals;
			wsn.observedValue=sdnode.observedValue;
			wsn.logProba=sdnode.logProba;
			wsn.isObserved=sdnode.isObserved;
			wsn.ic=sdnode.ic;
			wsn.dl=sdnode.dl;
			wsn.si=sdnode.si;
			ws.sdnodes.add(wsn);
		}
		return ws;
	}
}
