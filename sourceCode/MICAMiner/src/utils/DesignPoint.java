package utils;

import model.DistType;

public class DesignPoint {
	public String dagPath="CountsByCountry/DAG.txt";
	public String itemNamesPath="CountsByCountry/itemsNames.txt";
	public String sgAndSdnodeFilePath="CountsByCountry/usa_fr/probasModel.json";
	public String resultsPath="CountsByCountry/usa_fr/antichain.json";
	public DistType distType=DistType.binomial;
	
	public int maxScaleExponent=-1;
	public double scaleBase=2.;
	public double choiceNodeChild=0.1;
	//public double belongToAntichain=0.05;
	public double belongToAntichain=Math.pow(10, -100);
	public boolean accountForValueInDL=true;
	public int minXHat=4;
	public int maxPatternSize=6;
	public int nbMaxPatterns=10;
	//public boolean onlyUpdateTestings=false;
	
	public Statistics statistics=new Statistics();
	
	
}
