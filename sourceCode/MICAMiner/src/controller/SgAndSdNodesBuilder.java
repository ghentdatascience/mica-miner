package controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import model.Dag;
import model.Node;
import model.Pair;
import model.SdNode;
import model.Subgroup;
import utils.DesignPoint;

public class SgAndSdNodesBuilder {
	public Subgroup minedSg;
	public Subgroup beliefSg;
	public ArrayList<SdNode> allSdNodes;
	public HashMap<Node, SdNode> allSdNodesMap;
	
	public Dag dag;
	private String sgAndSdnodeFilePath;
	
	public SgAndSdNodesBuilder(Dag dag, String sgAndSdnodeFilePath) {
		this.dag = dag;
		this.sgAndSdnodeFilePath = sgAndSdnodeFilePath;
	}
	
	public void load(DesignPoint designPoint) {
		try {
			BufferedReader graphFile = new BufferedReader(new FileReader(new File(sgAndSdnodeFilePath)));
			String line;
			StringBuilder sb = new StringBuilder();
			while ((line = graphFile.readLine()) != null) {
				sb.append(line);
		        sb.append("\n");
			}
			graphFile.close();
			String fileAsString=sb.toString();
			JSONObject dataAsJson = new JSONObject(fileAsString);
			minedSg=new Subgroup(dag.allNodes.size());
			beliefSg=new Subgroup(dag.allNodes.size());
			allSdNodes=new ArrayList<>();
			allSdNodesMap=new HashMap<>();
			
			designPoint.maxScaleExponent=dataAsJson.getJSONObject("metadata").getInt("maxScaleExponent");
			
			for (int i=0;i<dataAsJson.getJSONArray("sdnodes").length();i++) {
				JSONObject jsonObj=dataAsJson.getJSONArray("sdnodes").getJSONObject(i);
				Node node=dag.nodesMap.get(jsonObj.getString("id"));
//				if (designPoint.maxScaleExponent==-1) {
//					designPoint.maxScaleExponent=jsonObj.getJSONArray("logProbaOfScales").length();
//					System.out.println("maxScaleComponent:"+designPoint.maxScaleExponent);
//				}
				Pair<Node,Double> p1=new Pair<Node, Double>(node, jsonObj.getDouble("xHat"));
				Pair<Node,Double> p2=new Pair<Node, Double>(node, jsonObj.getDouble("xBar"));
				minedSg.occuredNodes.fastSet(node.index);
				minedSg.overalNbNodesInDag++;
				minedSg.sumOfSupports+=p1.second;
				minedSg.supportList.add(p1);
				minedSg.supportMap.put(node, p1);
				
				beliefSg.occuredNodes.fastSet(node.index);
				beliefSg.overalNbNodesInDag++;
				beliefSg.sumOfSupports+=p2.second;
				beliefSg.supportList.add(p2);
				beliefSg.supportMap.put(node, p2);
				
				
				SdNode sdn=new SdNode();
				sdn.node=node;
				sdn.xHat=p1.second;
				sdn.xBar=p2.second;
				sdn.yHat=jsonObj.getDouble("yHat");
				sdn.yBar=jsonObj.getDouble("yBar");
				sdn.observedValue=(int)Math.floor(sdn.yHat);
				//sdn.geometricParam=1./(1.+sdn.xHat);
				sdn.marginProba=jsonObj.getDouble("marginProba");
				sdn.logProbaOfScales=new double[jsonObj.getJSONArray("logProbaOfScales").length()];
				for (int j=0;j<sdn.logProbaOfScales.length;j++) {
					sdn.logProbaOfScales[j]=jsonObj.getJSONArray("logProbaOfScales").getDouble(j);
				}
				sdn.logProbaOfScalesCondToParent=new double[sdn.logProbaOfScales.length][];
				for (int j=0;j<sdn.logProbaOfScales.length;j++) {
					sdn.logProbaOfScalesCondToParent[j]=new double[sdn.logProbaOfScales.length];
					for (int k=0;k<sdn.logProbaOfScales.length;k++) {
						sdn.logProbaOfScalesCondToParent[j][k]=jsonObj.getJSONArray("logProbaOfScalesCondToParent").getJSONArray(j).getDouble(k);
					}
				}
				allSdNodes.add(sdn);
				allSdNodesMap.put(node, sdn);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	
	
}
