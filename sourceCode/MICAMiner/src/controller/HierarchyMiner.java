package controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import model.DLType;
import model.Dag;
import model.Node;
import model.SdNode;
import model.SubDag;
import model.Subgroup;
import utils.MathOperations;

public class HierarchyMiner {
	private Dag dag;
	private Subgroup beliefSg;
	private Subgroup minedSg;
	public ArrayList<SdNode> allSdNodes;
	public HashMap<Node, SdNode> allSdNodesMap;
	private QualityComputer qualComputer;
	private DLType dlType;

	public HierarchyMiner(SgAndSdNodesBuilder sgAndSdNodesBuilder, QualityComputer qualComputer, DLType dlType) {
		this.dag = sgAndSdNodesBuilder.dag;
		this.beliefSg = sgAndSdNodesBuilder.beliefSg;
		this.minedSg = sgAndSdNodesBuilder.minedSg;
		this.allSdNodes = sgAndSdNodesBuilder.allSdNodes;
		this.allSdNodesMap = sgAndSdNodesBuilder.allSdNodesMap;
		this.qualComputer = qualComputer;
		this.dlType = dlType;
	}


	public void propagateMessages() {
		qualComputer.designPoint.statistics.nbUpdates++;
		long start=System.nanoTime()/10000000;
		SdNode rootSdn = allSdNodesMap.get(dag.root);
		rootSdn.computeAscendentMessages(allSdNodesMap, qualComputer.designPoint);
		rootSdn.computeDescendentMessages(allSdNodesMap, qualComputer.designPoint);
		for (SdNode sdn : allSdNodes) {
			sdn.computeLogMarginals(allSdNodesMap, qualComputer.designPoint, dlType, beliefSg, minedSg);
		}
		long end=System.nanoTime()/10000000;
		qualComputer.designPoint.statistics.updateTimeMS+=(end-start);
	}

	public SubDag getAllSdNodesInSubDag() {
		makeNodeObserved("0");
		propagateMessages();

		Collections.sort(allSdNodes, Collections.reverseOrder());
		SubDag subdag = new SubDag();
		subdag.sdnodes = allSdNodes;
		return subdag;
	}

	public void makeNodeObserved(String nodeId) {
		Node node = dag.nodesMap.get(nodeId);
		SdNode sdn = allSdNodesMap.get(node);
		sdn.isObserved = true;
	}

	public SubDag getAllSdNodesInSubDagWithUpdate() {
		SubDag subdag = new SubDag();
		subdag.sdnodes = new ArrayList<>();
		ArrayList<SdNode> curall = new ArrayList<>(allSdNodes);
		int cpt = 0;
		// makeNodeObserved("0");
		while (curall.size() > 0) {
			cpt++;
			System.out.println("iteration: " + cpt);
			propagateMessages();
			SdNode sdn = getBestSdNodeAndRemove(curall);
			subdag.sdnodes.add(sdn.copy());
			sdn.isObserved = true;
		}

		// propagateMessages();
		// Collections.sort(allSdNodes, Collections.reverseOrder());
		return subdag;
	}

	public SdNode getBestSdNodeAndRemove(ArrayList<SdNode> curall) {
		SdNode sdn = curall.get(0);
		int curIndex = 0;
		for (int i = 0; i < curall.size(); i++) {
			SdNode curS = curall.get(i);
			if (curS.si > sdn.si) {
				sdn = curS;
				curIndex = i;
			}
		}
		curall.remove(curIndex);
		return sdn;
	}

	public ArrayList<SubDag> getNotNecMaximalAntichains(){
		ArrayList<SubDag> antichains=new ArrayList<>();
		
		makeNodeObserved("0");
		propagateMessages();
		
		
		ArrayList<SdNode> nodes=new ArrayList<>(allSdNodes);
		nodes.remove(0);
		boolean continu=true;
		int nbAntichains=qualComputer.designPoint.nbMaxPatterns;
		while (continu) {
			if (nbAntichains!=-1 && antichains.size()>=nbAntichains) {
				break;
			}
			SubDag sd=getNotNecMaximalAntichain(new ArrayList<SdNode>(nodes));
			if (sd.sdnodes.size()==0) {
				continu=false;
			}
			else {
				ArrayList<SdNode> sdnodes=new ArrayList<>();
				for (SdNode sdn : sd.sdnodes) {
					sdnodes.add(sdn.copy());
					sdn.isObserved=true;
				}
				sd.sdnodes=sdnodes;
				antichains.add(sd);
				System.out.println("size of antichain nb "+antichains.size()+" is: "+sd.sdnodes.size());
				propagateMessages();
				for (SdNode sdn : nodes) {
					sdn.isCoveredWithAntichain=false;
				}
			}
		}
		return antichains;
	}
	
	public SubDag getNotNecMaximalAntichain(ArrayList<SdNode> remainingSdNodes) {
		// we use a the MaxAntichain DL
		SubDag sd = new SubDag();
		//ArrayList<SdNode> remainingSdNodes = new ArrayList<>(allSdNodes);
		for (SdNode sdn : allSdNodes) {
			sdn.updateAntichainDL(qualComputer.designPoint, allSdNodesMap);
		}
		// what is the best node to add ?
		boolean continu = true;
		
		while (continu) {
			if (remainingSdNodes.size() == 0) {
				continu = false;
			} else {
				boolean addit=true;
				SdNode bestSdNode = null;
				int bestIndex = -1;
				double bestSi = sd.si;
				int i = 0;
				while (i < remainingSdNodes.size()) {
					SdNode sdn = remainingSdNodes.get(i);
					if (sdn.isCoveredWithAntichain) {
						remainingSdNodes.remove(i);
					} else {
						double newSiBelong;
						double newSiNotBelong;
						newSiNotBelong = (sd.ic) / (sd.dl + sdn.dl-MathOperations.log2(1-qualComputer.designPoint.belongToAntichain));
						newSiBelong = (sd.ic + sdn.ic) / (sd.dl + sdn.dl-MathOperations.log2(qualComputer.designPoint.belongToAntichain)+MathOperations.log2(qualComputer.designPoint.maxScaleExponent));
						if (!sdn.isObserved && (sdn.xHat>=qualComputer.designPoint.minXHat || sdn.xBar>=qualComputer.designPoint.minXHat) &&(qualComputer.designPoint.maxPatternSize==-1 || qualComputer.designPoint.maxPatternSize>sd.sdnodes.size())) {
							if (bestIndex == -1 || newSiBelong > bestSi) {
								bestSi = newSiBelong;
								bestIndex = i;
								bestSdNode = sdn;
								addit=true;
							}
						}
						
						if ( newSiNotBelong > bestSi) {
							bestSi = newSiNotBelong;
							bestIndex = i;
							bestSdNode = sdn;
							addit=false;
						}
						i++;
					}
				}
				if (bestIndex == -1) {
					continu = false;
				} else {
					if (addit) {
						sd.sdnodes.add(bestSdNode);
						sd.ic += bestSdNode.ic;
						sd.dl += (bestSdNode.dl-MathOperations.log2(qualComputer.designPoint.belongToAntichain)+MathOperations.log2(qualComputer.designPoint.maxScaleExponent));
						sd.si = sd.ic / sd.dl;
					}
					else {
						sd.dl+=(bestSdNode.dl-MathOperations.log2(1-qualComputer.designPoint.belongToAntichain));
						sd.si = sd.ic / sd.dl;
					}
					
					remainingSdNodes.remove(bestIndex);
					bestSdNode.updateCoverageAntiChain(allSdNodesMap);
					for (SdNode sdn : remainingSdNodes) {
						sdn.updateAntichainDL(qualComputer.designPoint, allSdNodesMap);
					}
				}
			}
		}
		return sd;
	}
	
	
	
	
	


}
