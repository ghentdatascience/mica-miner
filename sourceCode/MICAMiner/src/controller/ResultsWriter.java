package controller;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import model.SubDag;
import utils.DesignPoint;
import utils.WritableResult;
import utils.WritableSubDag;

public class ResultsWriter {
	
	
	
	public static void writeSubDag(SubDag sdag, String filePath,DesignPoint designPoint) {
		WritableSubDag ws=WritableSubDag.create(sdag);
		WritableResult rs=new WritableResult();
		rs.metadata=designPoint;
		rs.patterns.add(ws);
		Gson gson;
		gson = new GsonBuilder().setPrettyPrinting().create();
		try (FileWriter writer = new FileWriter(
				filePath)) {
			gson.toJson(rs, writer);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static void writeSubDags(ArrayList<SubDag> sdags, String filePath,DesignPoint designPoint) {
		WritableResult rs=new WritableResult();
		rs.metadata=designPoint;
		for (SubDag sdag : sdags) {
			rs.patterns.add(WritableSubDag.create(sdag));
		}
		Gson gson;
		gson = new GsonBuilder().setPrettyPrinting().create();
		try (FileWriter writer = new FileWriter(
				filePath)) {
			gson.toJson(rs, writer);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
