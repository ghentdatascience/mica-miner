package model;

import java.util.ArrayList;
import java.util.HashSet;

import org.apache.lucene.util.OpenBitSet;

public class Node {
	public int index;
	public String id;
	public String name;
	public ArrayList<Node> directDesc=new ArrayList<>();
	public HashSet<Node> allDesc=null;
	public OpenBitSet allDescBitSet=null;
	//public ArrayList<Node> directAsc=new ArrayList<>();
	public Node directParent=null;
	public int childIndex=-1;
	public HashSet<Node> allAsc=null;
	
	
	public HashSet<Node> computeAllDesc(int overallSize){
		if (allDesc!=null) {
			return allDesc;
		}
		allDesc=new HashSet<>();
		for (Node node : directDesc) {
			allDesc.add(node);
			allDesc.addAll(node.computeAllDesc(overallSize));
		}
		allDescBitSet=new OpenBitSet(overallSize);
		for (Node n : allDesc) {
			allDescBitSet.fastSet(n.index);
		}
		return allDesc;
	}
	public HashSet<Node> computeAllAsc(int overallSize){
		if (allAsc!=null) {
			return allAsc;
		}
		allAsc=new HashSet<>();
		if (directParent!=null) {
			allAsc.add(directParent);
			allAsc.addAll(directParent.computeAllAsc(overallSize));
		}
		
		return allAsc;
	}
	
	public void setChildrenIndices() {
		for (int i=0;i<directDesc.size();i++) {
			directDesc.get(i).childIndex=i;
		}
		for (int i=0;i<directDesc.size();i++) {
			directDesc.get(i).setChildrenIndices();
		}
	}
	
	@Override
	public String toString() {
		return name;
	}
}
