package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import utils.DesignPoint;
import utils.MathOperations;

public class SdNode implements Comparable<SdNode> {
	public Node node;
	public double xHat; // true value
	public double xBar; // init expected value
	public double yBar; // init log expected value
	public double yHat; // log true value
	public int observedValue;

	public double[][] logProbaOfScalesCondToParent = null;
	public double[] logProbaOfScales = null;
	public double marginProba = 0;
	public double currentMode;

	public double sumOfMarginals = 0;
	public double[] logMarginals = null;
	public double[] logMuThisToFp = null;
	public ArrayList<double[]> logMuFcToThisList = null;
	public double[] logMuFpToThis = null;
	public ArrayList<double[]> logMuThisToFcList = null;
	public double logProba = -1;
	public double ic;
	public double dl;
	public double si;
	public boolean isObserved = false;
	private double[] temp = null;

	public boolean isCoveredWithAntichain = false;

	public void computeAscendentMessages(HashMap<Node, SdNode> allSdNodesMap, DesignPoint designPoint) {
		if (temp == null) {
			temp = new double[designPoint.maxScaleExponent];
		}
		if (node.name.equals("College Basketball Court") || node.name.equals("College Stadium")) {
			int kk = 1;
			kk++;
		}

		if (logMuThisToFp == null) {
			logMuThisToFp = new double[designPoint.maxScaleExponent];
		}
		// if this is a leaf then it is 1
		if (node.directDesc.size() == 0) {
			for (int i = 0; i < logMuThisToFp.length; i++) {
				logMuThisToFp[i] = 0;
			}
		} else {
			// it is the product of muFcToThis
			boolean firstTime = false;
			if (logMuFcToThisList == null) {
				logMuFcToThisList = new ArrayList<>();
				firstTime = true;
			}
			for (int i = 0; i < node.directDesc.size(); i++) {

				double[] muFcToThis = null;
				if (allSdNodesMap.containsKey(node.directDesc.get(i))) {
					if (firstTime) {
						muFcToThis = new double[designPoint.maxScaleExponent];
					} else {
						muFcToThis = logMuFcToThisList.get(i);
					}
					SdNode childSdn = allSdNodesMap.get(node.directDesc.get(i));
					childSdn.computeAscendentMessages(allSdNodesMap, designPoint);

					for (int k = 0; k < designPoint.maxScaleExponent; k++) {
						muFcToThis[k] = 0;
						// if (!childSdn.isObserved) {
						double maxVal = -Double.MAX_VALUE;
						int indexMax = 0;
						for (int j = 0; j < designPoint.maxScaleExponent; j++) {
							temp[j] = childSdn.getFactorValue(j, k, false) + childSdn.logMuThisToFp[j];
							if (temp[j] > maxVal) {
								maxVal = temp[j];
								indexMax = j;
							}
						}
						muFcToThis[k] = maxVal;
						if (maxVal != -Double.MAX_VALUE) {
							double tmp2 = 0;
							for (int j = 0; j < designPoint.maxScaleExponent; j++) {
								if (j != indexMax) {
									tmp2 += Math.exp(temp[j] - maxVal);
								}
							}
							tmp2++;
							muFcToThis[k] += Math.log(tmp2);
						}
						if (Double.isInfinite(muFcToThis[k])) {
							throw new RuntimeException("infinity");
						}
						// }
						// else {
						// muFcToThis[k] += (childSdn.getFactorV(childSdn.observedValue, k, this,
						// designPoint) * childSdn.muThisToFp[j]);
						// }

					}
				}
				if (firstTime) {
					logMuFcToThisList.add(muFcToThis);
				}

			}
			for (int i = 0; i < designPoint.maxScaleExponent; i++) {
				logMuThisToFp[i] = 0;
				for (int j = 0; j < node.directDesc.size(); j++) {
					if (allSdNodesMap.containsKey(node.directDesc.get(j))) {
						logMuThisToFp[i] += logMuFcToThisList.get(j)[i];
					}
				}
				if (Double.isInfinite(logMuThisToFp[i])) {
					logMuThisToFp[i] = -Double.MAX_VALUE;
					// throw new RuntimeException("infinity");
				}
			}
		}
	}

	public void computeDescendentMessages(HashMap<Node, SdNode> allSdNodesMap, DesignPoint designPoint) {
		// compute muFpToThis
		if (node.name.equals("College Basketball Court") || node.name.equals("College Stadium")) {
			int kk = 1;
			kk++;
		}
		if (temp == null) {
			temp = new double[designPoint.maxScaleExponent];
		}
		if (logMuFpToThis == null) {
			logMuFpToThis = new double[designPoint.maxScaleExponent];
		}
		if (node.directParent == null) {
			// this is the root
			for (int i = 0; i < designPoint.maxScaleExponent; i++) {
				logMuFpToThis[i] = getFactorValue(i, 0, true);
				if (Double.isInfinite(logMuFpToThis[i])) {
					logMuFpToThis[i] = -Double.MAX_VALUE;
					throw new RuntimeException("infinity");
				}
			}
		} else {
			for (int i = 0; i < designPoint.maxScaleExponent; i++) {
				logMuFpToThis[i] = 0;
				SdNode parent = allSdNodesMap.get(node.directParent);

				double maxVal = -Double.MAX_VALUE;
				int indexMax = 0;

				for (int j = 0; j < designPoint.maxScaleExponent; j++) {
					temp[j] = getFactorValue(i, j, false) + parent.logMuThisToFcList.get(node.childIndex)[j];
					if (temp[j] > maxVal) {
						maxVal = temp[j];
						indexMax = j;
					}
					// logMuFpToThis[i] += getFactorValue(i, j, false) *
					// parent.logMuThisToFcList.get(node.childIndex)[j];
				}
				logMuFpToThis[i] = maxVal;
				if (maxVal != -Double.MAX_VALUE) {
					double tmp2 = 0;
					for (int j = 0; j < designPoint.maxScaleExponent; j++) {
						if (j != indexMax) {
							tmp2 += Math.exp(temp[j] - maxVal);
						}
					}
					tmp2++;
					logMuFpToThis[i] += Math.log(tmp2);
				}
				if (Double.isInfinite(logMuFpToThis[i])) {
					throw new RuntimeException("infinity");
				}
			}
		}
		// compute muThisToFc
		boolean firstTime = false;
		if (logMuThisToFcList == null) {
			logMuThisToFcList = new ArrayList<>();
			firstTime = true;
		}
		for (int i = 0; i < node.directDesc.size(); i++) {
			double[] muThisToFc = null;
			if (allSdNodesMap.containsKey(node.directDesc.get(i))) {
				if (firstTime) {
					muThisToFc = new double[designPoint.maxScaleExponent];
				} else {
					muThisToFc = logMuThisToFcList.get(i);
				}
				for (int k = 0; k < designPoint.maxScaleExponent; k++) {
					muThisToFc[k] = logMuFpToThis[k];
					for (int j = 0; j < node.directDesc.size(); j++) {
						if (allSdNodesMap.containsKey(node.directDesc.get(j))) {
							if (j != i) {
								muThisToFc[k] += logMuFcToThisList.get(j)[k];
							}
						}
					}
					if (Double.isInfinite(muThisToFc[k])) {
						muThisToFc[k] = -Double.MAX_VALUE;
						// throw new RuntimeException("infinity");
					}
				}
			}
			if (firstTime) {
				logMuThisToFcList.add(muThisToFc);
			}
		}
		for (int i = 0; i < node.directDesc.size(); i++) {
			SdNode childSdn = allSdNodesMap.get(node.directDesc.get(i));
			if (childSdn != null) {
				childSdn.computeDescendentMessages(allSdNodesMap, designPoint);
			}
		}
	}

	public void computeLogMarginals(HashMap<Node, SdNode> allSdNodesMap, DesignPoint designPoint, DLType dlt,
			Subgroup beliefSg, Subgroup minedSg) {
		if (node.name.equals("College Basketball Court") || node.name.equals("College Stadium")) {
			int kk = 1;
			kk++;
		}
		if (logMarginals == null) {
			logMarginals = new double[designPoint.maxScaleExponent];
		}
		double sum = 0;
		currentMode = 0;
		double maxVal = -Double.MAX_VALUE;
		int indexMax = 0;
		for (int i = 0; i < designPoint.maxScaleExponent; i++) {
			logMarginals[i] = logMuFpToThis[i];
			for (int j = 0; j < node.directDesc.size(); j++) {
				if (allSdNodesMap.containsKey(node.directDesc.get(j))) {
					logMarginals[i] += logMuFcToThisList.get(j)[i];
				}
			}
			if (Double.isInfinite(logMarginals[i])) {
				logMarginals[i] = -Double.MAX_VALUE;
			}
			if (logMarginals[i] > maxVal) {
				maxVal = logMarginals[i];
				indexMax = i;
			}
			// sum += logMarginals[i];
			// currentMean += logMarginals[i] * ((double) i);
		}
		currentMode = indexMax;
		sum = maxVal;
		double tmp2 = 0;
		for (int i = 0; i < designPoint.maxScaleExponent; i++) {
			if (i != indexMax) {
				tmp2 += Math.exp(logMarginals[i] - maxVal);
			}
		}
		tmp2++;
		sum += Math.log(tmp2);
		if (sum > 0) {
			throw new RuntimeException();
		}
		for (int i = 0; i < designPoint.maxScaleExponent; i++) {
			logMarginals[i] -= sum;
			// currentMode += Math.exp(logMarginals[i]) * ((double) i);
		}

		// compute sum

		sumOfMarginals = 1;
		logProba = logMarginals[observedValue];
		// if (logProba == -Double.MAX_VALUE) {
		// throw new RuntimeException("-double max value");
		// }
		// if (logProba == 0) {
		// throw new RuntimeException("this proba is null");
		// logProba = Double.MIN_VALUE;
		// }

		ic = -logProba / Math.log(2);
		if (Double.isInfinite(ic)) {
			ic = Double.MAX_VALUE;
		}

		// sdNode.dl=-MathOperations.log2(groundTruth.supportMap.get(sdNode.node).second/groundTruth.sumOfSupports);
		if (dlt == DLType.uniform) {
			dl = MathOperations.log2(beliefSg.supportList.size());
		} else {
			dl = -MathOperations.log2(minedSg.supportMap.get(node).second / minedSg.sumOfSupports);
		}
		si = ic / dl;
	}

	public void updateAntichainDL(DesignPoint designPoint, HashMap<Node, SdNode> allSdNodesMap) {
		if (isCoveredWithAntichain) {
			dl = Double.MAX_VALUE;
		} else {
			dl = 0;
			if (designPoint.accountForValueInDL) {
				dl=MathOperations.log2(designPoint.maxScaleExponent);
			}
			if (node.directDesc.size() > 0) {
				dl -= MathOperations.log2(1.-designPoint.choiceNodeChild);
			}
			boolean continu = true;
			SdNode curN = this;
			while (continu) {
				curN = allSdNodesMap.get(curN.node.directParent);
				if (curN != null && !curN.isCoveredWithAntichain) {
					dl -= MathOperations.log2(designPoint.choiceNodeChild);
				} else {
					continu = false;
				}
			}
		}
		//si=ic/dl;
	}
	
	
	
	public void updateCoverageAntiChain(HashMap<Node, SdNode> allSdNodesMap) {
		isCoveredWithAntichain=true;
		boolean continu=true;
		SdNode curN=this;
		while (continu) {
			curN = allSdNodesMap.get(curN.node.directParent);
			if (curN != null && !curN.isCoveredWithAntichain) {
				curN.isCoveredWithAntichain=true;
			}
			else {
				continu=false;
			}
		}
		for (Node nd : node.allDesc) {
			if (allSdNodesMap.containsKey(nd)) {
				allSdNodesMap.get(nd).isCoveredWithAntichain=true;
			}
		}
	}

	public double getFactorValue(int value, int parentValue, boolean isRoot) {
		if (isObserved && value != observedValue) {
			return -Double.MAX_VALUE;
		}
		if (isRoot) {
			return logProbaOfScales[value];
		} else {
			return logProbaOfScalesCondToParent[value][parentValue];
		}
	}

	@Override
	public int compareTo(SdNode o) {
		if (xHat != xBar && o.xHat == o.xBar) {
			return 1;
		}
		if (xHat == xBar && o.xHat != o.xBar) {
			return -1;
		}
		if (si > o.si)
			return 1;
		if (si < o.si)
			return -1;
		return 0;
	}

	public SdNode copy() {
		SdNode sdn = new SdNode();
		sdn.node = node;
		sdn.xHat = xHat;
		sdn.xBar = xBar;
		sdn.yBar = yBar;
		sdn.yHat = yHat;
		sdn.observedValue = observedValue;
		sdn.marginProba = marginProba;
		sdn.currentMode = currentMode;
		sdn.sumOfMarginals = sumOfMarginals;
		sdn.logMarginals = Arrays.copyOf(logMarginals, logMarginals.length);
		sdn.logProba = logProba;
		sdn.ic = ic;
		sdn.dl = dl;
		sdn.si = si;
		sdn.isObserved = isObserved;
		return sdn;
	}
}
