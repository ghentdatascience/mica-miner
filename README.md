# Contrastive antichains in hierarchies

This repository provides source code, data, and some results for the following paper: 

Anes Bendimerad, Jefrey Lijffijt, Marc Plantevit, Céline Robardet, and Tijl De Bie. 2019. Contrastive antichains in hierarchies. In The 25th ACM SIGKDD Conference on Knowledge Discovery and Data Mining (KDD ’19).

## Data:
The "Data" folder contains the input files required for running MICAMiner on the studied datasets: Foursquare, OpenFoodFact, EuropeanParilament.
As an example, we will explain the inputs corresponding to the Foursquare Amsterdam dataset (in the folder "Data/FoursquareAmsterdam"), the other cases follow the same structure.

- itemNames.txt: it contains the id and the corresponding name of each item (e.g., 4d4b7104d754a06370d81259,Arts & Entertainment)
- DAG.txt: it gives the hierarchy structure, i.e., each line of this files depicts a parent child relation based on ids of items.
- supportAmsterdam.txt: contains the observed value of each item (the xHat values).
- supportAll.txt: contains the values used to infer the expected values of each item (they give xBar after normalization by the total number of venues in Amsterdam).
- probasModel.json: this file contains the values of factor functions that are used by MICAMiner, this file is produced based on the algorithm "FactorsComputer" whose code is given in "sourceCode/FactorsComputer".

## Algorithms:
**FactorsComputer**: this algorithm allows to compute the values of factor functions that are required by MICAMiner algorithm to run. We give in "sourceCode/FactorsComputer" its code and runnable file. FactorsComputer requires a parameter file, an example of this file is given in "sourceCode/FactorsComputer/parameters.txt":

```

dagPath=../../Data/Foursquare/Amsterdam/DAG.txt

itemNamesPath=../../Data/Foursquare/Amsterdam/itemsNames.txt

beliefSgPath=../../Data/Foursquare/Amsterdam/supportAll.txt

minedSgPath=../../Data/Foursquare/Amsterdam/supportAmsterdam.txt

resultsPath=../../Data/Foursquare/Amsterdam/probasModel.json

maxX=100000

```

In order to run this algorithm, the following command line can be executed from the "sourceCode/FactorsComputer" folder:

```

java -jar FactorsComputer.jar parameters.txt

```


**MICAMiner**: this is the main algorithm, it allows to find the contrastive antichains. We give in "sourceCode/MICAMiner" its code and runnable file. MICAMiner requires a parameter file, an example of this file is given in "sourceCode/MICAMiner/parameters.txt":

```

dagPath=../../Data/Foursquare/Amsterdam/DAG.txt

itemNamesPath=../../Data/Foursquare/Amsterdam/itemsNames.txt

sgAndSdnodeFilePath=../../Data/Foursquare/Amsterdam/probasModel.json

resultsPath=../../Data/Foursquare/Amsterdam/antichains.json

choiceNodeChild=0.2

belongToAntichain=1.0E-50

maxPatternSize=6

nbMaxPatterns=20

```

The parameters choiceNodeChild and belongToAntichain correspond respectively to alpha1 and alpha2. To run this algorithm, the following command line can be executed from the "sourceCode/MICAMiner" folder:


```

java -jar MICAMiner.jar parameters.txt

```


## Results:
In the folder "Antichains" contains the results for each dataset. For each case, we provide the output file and the figures.


