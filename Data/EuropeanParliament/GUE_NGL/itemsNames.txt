0,root
1,3 COMMUNITY POLICIES
2,3.40 Industrial policy
3,3.40.02 Iron and steel industry, metallurgical industry
4,4 Economic, social and territorial cohesion
5,4.15 Employment policy, action to combat unemployment
6,4.15.05 Industrial restructuring, job losses, redundancies, relocations, Globalisation Adjustment Fund (EGF)
7,8 State and evolution of the Union
8,8.70 Budget of the Union
9,8.70.54 2014 budget
10,3.40.03 Motor industry, cycle and motorcycle, commercial and agricultural vehicles
11,8.70.53 2013 budget
12,8.70.55 2015 budget
13,6 External relations of the Union
14,6.20 Common commercial policy in general
15,6.20.03 Bilateral economic and trade agreements and relations
16,5 ECONOMIC AND MONETARY SYSTEM
17,5.10 Economic union
18,5.10.01 Convergence of economic policies, public deficit, interest rates
19,6.20.04 Union Customs Code, tariffs, preferential arrangements, rules of origin
20,8.70.03 Budgetary control and discharge, implementation of the budget
21,8.70.03.02 2012 discharge
22,3.40.12 Luxury products industry, cosmetics
23,3.40.17 Manufactured goods
24,6.40 Relations with third countries
25,6.40.15 European neighbourhood policy
26,3.20 Transport policy in general
27,3.20.02 Rail transport: passengers and freight
28,7 Area of freedom, security and justice
29,7.40 Judicial cooperation
30,7.40.02 Judicial cooperation in civil and commercial matters
31,6.20.05 Multilateral economic and trade agreements and relations
32,3.40.04 Shipbuilding, nautical industry
33,3.40.13 Food industry
34,6.30 Development cooperation
35,2 Internal market, single market
36,2.50 Free movement of capital
37,2.50.10 Financial supervision
38,5.20 Monetary union
39,5.20.03 European Central Bank (ECB), ESCB
40,3.50 Research and technological development RTD
41,3.50.01 European research area and policy
42,3.50.01.05 Research specific areas
43,4.60 Consumers' protection in general
44,4.60.04 Consumer health
45,4.60.04.04 Food safety
46,3.70 Environmental policy
47,3.70.01 Protection of natural resources: fauna, flora, nature, wildlife, countryside; biodiversity
48,3.70.18 International and regional environment protection measures and agreements
49,3.30 Information and communications in general
50,3.30.03 Telecommunications, data transmission, telephone
51,3.30.03.06 Communications by satellite
52,3.40.05 Aeronautical industry, aerospace industry
53,3.50.20 Scientific and technological cooperation and agreements
54,6.40.01 Relations with EEA/EFTA countries
55,2.70 Taxation
56,2.70.02 Indirect taxation, VAT, excise duties
57,4.70 Regional policy
58,4.70.06 Outlying and outermost regions, overseas countries and territories
59,3.40.06 Electronics, electrotechnical industries, ICT, robotics
60,3.20.01 Air transport and air freight
61,3.10 Agricultural policy and economies
62,3.10.06 Crop products in general, floriculture
63,3.10.06.01 Fruit, citrus fruits
64,3.70.11 Natural disasters, Solidarity Fund
65,8.70.01 Financing of the budget, own resources
66,8.70.70 Flexibility instrument
67,3.15 Fisheries policy
68,3.15.01 Fish stocks, conservation of fishery resources
69,3.15.04 Management of fisheries, fisheries, fishing grounds
70,3.15.15 Fisheries agreements and cooperation
71,3.15.15.02 Fisheries agreements with African countries
72,6.40.07 Relations with African countries
73,3.15.15.03 Fisheries agreements with Indian Ocean countries
74,6.40.05 Relations with the Mediterranean and Southern Europe countries
75,6.40.05.02 Relations with the countries of the Great Maghreb and Maghreb
76,3.10.09 Plant health legislation, organic farming, agro-genetics in general
77,3.10.09.06 Agro-genetics, GMOs
78,3.10.10 Foodstuffs, foodstuffs legislation
79,1 European citizenship
80,1.20 Citizen's rights
81,1.20.04 European Ombudsman
82,6.20.01 Agreements and relations in the context of the World Trade Organization (WTO)
83,6.20.02 Export/import control, trade defence
84,4.10 Social policy, social charter and protocol
85,4.10.02 Family policy, family law, parental leave
86,4.10.03 Child protection, children's rights
87,8.40 Institutions of the Union
88,8.40.01 European Parliament
89,8.40.01.02 President, members, mandates, political groups
90,1.20.09 Protection of privacy and data protection
91,3.20.06 Transport regulations, road safety, roadworthiness tests, driving licence
92,8.40.13 ACP-EU bodies
93,3.40.01 Chemical industry, fertilizers, plastics
94,3.40.08 Mechanical engineering, machine-tool industry
95,2.50.03 Securities and financial markets, stock exchange, CIUTS, investments
96,2.50.05 Insurance, pension funds
97,2.50.08 Financial services, financial reporting and auditing
98,2.80 Cooperation between administrations
99,8.40.08 Agencies and bodies of the EU
100,2.50.04 Banks and credit
101,2.50.04.02 Electronic money and payments, cross-border credit transfers
102,3.45 Enterprise policy, inter-company cooperation
103,3.45.03 Financial management of undertakings, business loans, accounting
104,3.45.05 Business policy, electronic commerce, after-sales service, commercial distribution
105,4.60.06 Consumers' economic and legal interests
106,4.10.04 Gender equality
107,2.60 Competition
108,6.40.03 Relations with South-East Europe and the Balkans
109,8.20 Enlargement of the Union
110,8.20.01 Candidate countries
111,8.20.04 Pre-accession and partnership
112,3.10.05 Livestock products, in general
113,3.10.05.01 Meat
114,3.10.06.03 Cereals, rice
115,8.70.56 2016 budget
116,8.70.04 Action to combat Community fraud
117,5.05 Economic growth
118,6.10 Common foreign and security policy (CFSP)
119,6.10.08 Fundamental freedoms, human rights, democracy in general
120,6.10.09 Human rights situation in the world
121,3.30.01 Audiovisual industry and services
122,3.30.08 Press, media freedom and pluralism
123,3.40.18 Services sector
124,6.20.07 Macro-financial assistance to third countries
125,3.20.05 Road transport: passengers and freight
126,4.10.10 Social protection, social security
127,4.15.02 Employment: guidelines, actions, Funds
128,8.70.02 Financial regulations
129,3.15.08 Fishing enterprises, fishermen, working conditions on board
130,3.20.15 Transport agreements and cooperation
131,3.20.15.06 Maritime or inland transport agreements and cooperation
132,4.15.06 Professional qualifications, recognition of qualifications
133,3.60 Energy policy
134,3.60.02 Oil industry, motor fuels
135,3.60.05 Alternative and renewable energies
136,3.70.02 Atmospheric pollution, motor vehicle pollution
137,3.70.03 Climate policy, climate change, ozone layer
138,3.70.12 Waste management, domestic waste, packaging, light industrial waste
139,4.60.02 Consumer information, advertising, labelling
140,6.40.04 Relations with the Commonwealth of Independent States (CIS)
141,6.40.04.02 Relations with Russian Federation
142,4.40 Education, vocational training and youth
143,4.40.01 European area for education, training and lifelong learning
144,4.40.04 Universities, higher education
145,4.40.20 Cooperation and agreements in the fields of education, training and youth
146,4.45 Common cultural area, cultural diversity
147,4.45.08 Cultural and artistic activities, books and reading, arts
148,3.10.11 Forestry policy
149,4.10.15 European Social Fund (ESF), Fund for European Aid to the Most Deprived (FEAD)
150,4.40.10 Youth
151,2.60.03 State aids and interventions
152,8.70.03.03 2013 discharge
153,3.40.10 Textile and clothing industry, leathers
154,7.40.04 Judicial cooperation in criminal matters
155,8.40.07 European Investment Bank
156,3.30.02 Television, cable, digital, mobile
157,3.30.04 Radiocommunications, broadcasting
158,3.30.25 International information networks and society, internet
159,3.50.15 Intellectual property, copyright
160,6.30.02 Financial and technical cooperation and assistance
161,4.20 Public health
162,4.20.01 Medicine, diseases
163,4.20.06 Health services, medical institutions
164,4.20.07 Medical and para-medical professions
165,4.60.08 Safety of products and services, product liability
166,3.40.14 Industrial competitiveness
167,3.45.02 Small and medium-sized enterprises (SME), craft industries
168,3.70.20 Sustainable development
169,3.40.16 Raw materials
170,6.10.05 Peace preservation, humanitarian and rescue tasks, crisis management
171,6.10.02 Common security and defence policy; WEU, NATO
172,3.40.09 Defence and arms industry
173,3.10.03 Marketing and trade of agricultural products and livestock
174,3.10.05.02 Milk and dairy products
175,3.10.06.02 Vegetables
176,4.40.03 Primary and secondary school, European Schools
177,3.10.14 Support for producers and premiums
178,3.50.16 Industrial property, European patent, Community patent, design and pattern
179,7.30 Police, judicial and customs cooperation in general
180,7.30.30 Action to combat crime
181,7.30.30.10 Action against counterfeiting
182,7.30.30.06 Action to combat economic fraud
183,8.40.09 European officials, EU servants, staff regulations
184,3.60.10 Security of energy supply
185,6.10.04 Third-country political situation, local and regional conflicts
186,4.70.01 Structural funds, investment funds in general, programmes
187,6.40.13 Relations with/in the context of international organisations: UN, OSCE, OECD, Council of Europe, EBRD
188,3.20.07 Combined transport, multimodal transport
189,7.10 Free movement and integration of third-country nationals
190,7.10.08 Migration policy
191,6.20.06 Foreign direct investment (FDI)
192,3.50.02 Framework programme and research programmes
193,3.50.02.01 EC, EU framework programme
194,3.50.04 Innovation
195,3.45.01 Company law
196,3.20.03 Maritime transport: passengers and freight
197,3.20.10 Transport undertakings, transport industry employees
198,4.15.10 Worker information, participation, trade unions, works councils
199,4.15.12 Workers protection and rights, labour law
200,3.10.08 Animal health requirements, veterinary legislation and pharmacy
201,3.10.08.01 Feedingstuffs, animal nutrition
202,3.60.04 Nuclear energy, industry and safety
203,3.70.08 Radioactive pollution
204,3.70.10 Man-made disasters, industrial pollution and accidents
205,6.40.05.06 Relations with the countries of the Middle East
206,2.10 Free movement of goods
207,2.10.01 Customs union, tax and duty-free, Community transit
208,7.30.02 Customs cooperation
209,3.10.04 Livestock farming
210,3.10.04.02 Animal protection
211,4.20.02 Medical research
212,4.20.02.04 Genetics and bioethics
213,1.10 Fundamental rights in the EU, Charter
214,8.40.01.06 Committees, interparliamentary delegations
215,8.40.03 European Commission
216,3.15.02 Aquaculture
217,3.70.05 Marine and coastal pollution, pollution from ships, oil pollution
218,3.45.06 Entrepreneurship, liberal professions
219,4.45.06 Heritage and culture protection, movement of works of art
220,3.70.04 Water control and management, pollution of waterways, water pollution
221,3.20.04 Inland waterway transport
222,7.10.06 Asylum, refugees, displaced persons; Asylum, Migration and Integration Fund (AMIF)
223,4.70.02 Cohesion policy, Cohesion Fund (CF)
224,4.70.04 Town and country planning
225,4.70.05 Regional cooperation, transfrontier cooperation
226,4.10.05 Social inclusion, poverty, minimum income
227,4.10.07 The elderly
228,4.10.11 Retirement, pensions
229,4.10.14 Demography
230,4.15.04 Workforce, occupational mobility, job conversion, working conditions
231,3.20.11 Trans-European transport networks
232,4.15.08 Work, employment, wages and salaries: equal opportunities women and men, and for all
233,4.10.09 Women condition and rights
234,8.50 EU law
235,8.50.01 Implementation of EU law
236,7.30.30.04 Action to combat drugs and drug-trafficking
237,2.10.03 Standardisation, EC standards and trademark, certification, compliance
238,3.15.17 European Maritime and Fisheries Fund (EMFF)
239,4.70.07 European Regional Development Fund (ERDF)
240,2.10.02 Public procurement
241,7.10.04 External borders crossing and controls, visas
242,3.10.06.07 Sugar
243,3.10.07 Animal and vegetable fats, oils
244,2.50.02 Savings
245,2.70.01 Direct taxation
246,4.50 Tourism
247,2.40 Free movement of services, freedom to provide
248,3.30.03.04 Telecommunication networks
249,3.30.05 Electronic and mobile communications, personal communications
250,3.30.06 Information and communication technologies
251,3.30.20 Trans-European communications networks
252,3.45.04 Company taxation
253,7.30.05 Police cooperation
254,7.30.05.01 Europol, CEPOL
255,3.20.01.01 Air safety
256,2.40.02 Public services, of general interest, universal service
257,8.40.01.01 Elections, direct universal suffrage
258,7.10.02 Schengen area
259,2.40.01 Right of establishment
260,6.10.01 Foreign and common diplomatic policy
261,7.30.20 Action to combat terrorism
262,4.15.15 Health and safety at work, occupational medicine
263,3.20.08 Urban transport
264,3.60.03 Gas, electricity, natural gas, biogas
265,3.60.06 Trans-European energy networks
266,6.40.10 Relations with Latin America, Central America, Caribbean islands
267,6.40.09 Relations with Oceanian countries
268,6.40.08 Relations with Asian countries
269,3.15.07 Fisheries inspectorate, surveillance of fishing vessels and areas
270,6.50 Emergency, food, humanitarian aid, aid to refugees, Emergency Aid Reserve
271,6.40.17 Relations with BRIC countries
272,6.10.03 Armaments control, non-proliferation nuclear weapons
273,3.15.05 Fish catches, import tariff quotas
274,1.20.03 Right of petition
275,3.70.09 Transfrontier pollution
276,6.40.02 Relations with central and eastern Europe
277,4.15.14 Social dialogue, social partners
278,2.30 Free movement of workers
279,3.10.08.05 Animal diseases
280,3.20.09 Ports policy
281,5.10.02 Price policy, price stabilisation
282,8.60 European statistical legislation
283,4.20.04 Pharmaceutical products and industry
284,8.70.57 2017 budget
285,8.50.02 Legislative simplification, coordination, codification
286,4.20.05 Health legislation and policy
287,6.40.16 Relations with international financial organisations: World Bank, IBRD, IMF
288,6.30.01 Generalised scheme of tariff preferences (GSP), rules of origin
289,6.40.06 Relations with ACP countries, conventions and generalities
290,3.15.15.08 Fisheries agreements with Northern and Baltic countries
291,3.20.15.02 Air transport agreements and cooperation
292,8.40.10 Interinstitutional relations, democratic deficit, subsidiarity, comitology
293,6.40.04.06 Relations with central Asian countries
294,1.20.05 Public access to information and documents, administrative practice
295,3.10.01 Agricultural structures and holdings, farmers
296,3.10.01.06 Less-favoured agricultural areas
297,3.50.06 Research staff, researchers
298,4.40.06 Teachers, trainers, pupils, students
299,7.30.30.02 Action to combat violence, trafficking in human beings and migrant smuggling
300,3.10.06.09 Industrial plants, tobacco, hops
301,2.60.01 Trade restrictions, concerted practices, dominant positions
302,3.50.08 New technologies, biotechnology
303,4.20.03 Drug addiction, alcoholism, smoking
304,3.20.03.01 Maritime safety
305,3.70.16 Law and environment, liability
306,2.20 Free movement of persons
307,8.40.04 Court of Justice, Court of First Instance
308,6.40.05.04 Relations with the countries of the Mashreq
309,3.60.08 Energy efficiency
310,4.10.06 People with disabilities
311,3.60.12 Energy statistics
312,3.40.07 Building industry
313,4.15.03 Arrangement of working time, work schedules
314,3.30.09 Postal services, parcel delivery services
315,7.30.30.08 Capital outflow, money laundering
316,3.60.15 Cooperation and agreements for energy
317,3.10.01.02 Rural development, European Agricultural Fund for Rural Development (EAFRD)
318,6.40.11 Relations with industrialised countries
319,8.70.60 Previous annual budgets
320,8.40.01.08 Business of Parliament, procedure, sittings, rules of procedure
321,3.50.02.03 Framework programme and research programme for Coal and Steel
322,4.10.13 Sport
323,3.15.15.06 Fisheries agreements with Pacific countries
324,8.10 Revision of the Treaties, intergovernmental conferences
325,8.30 Treaties in general
326,5.20.02 Single currency, euro, euro area
327,3.60.01 Solid fuels, coal mining, mining industry
328,3.70.13 Dangerous substances, toxic and radioactive wastes (storage, transport)
329,7.30.12 Control of personal weapons and ammunitions
330,8.70.58 2018 budget
331,3.15.06 Fishing industry and statistics, fishery products
332,3.15.03 Fishing fleets, safety of fishing vessels
333,3.10.06.10 Tropical plants
334,3.40.11 Precision engineering, optics, photography, medical
335,4.45.02 Cultural programmes and actions, assistance
336,8.70.03.05 2015 discharge
337,3.10.06.05 Textile plants, cotton
338,3.30.25.02 Information programmes and action plans
339,2.60.04 Economic concentration, mergers, takeover bids, holding companies
340,4.45.10 Literary and artistic property
341,3.10.14.04 Set-aside and agricultural reconversion
342,3.45.07 Social economy, mutual societies, cooperatives, associations
343,4.40.15 Vocational education and training
344,3.30.07 Cybersecurity, cyberspace policy
345,3.10.06.08 Wine, alcoholic and non-alcoholic beverages
346,4.10.08 Equal treatment between persons, anti-discrimination
347,7.30.08 Action to combat racism and xenophobia
348,6.40.04.04 Relations with Caucasus countries
349,4.40.07 Recognition of diplomas, equivalence of studies and training
350,6.30.04 Loans to third-countries, Guarantee Fund
351,3.10.30 Agricultural statistics
352,8.70.59 2019 budget
353,3.10.06.04 Fodder plants
354,3.10.06.06 Oleaginous plants
355,3.50.02.02 Euratom framework programme
356,4.40.08 Language learning, regional and local languages
357,8.30.10 Principles common to the Member States, EU values
